import React from 'react';
import './App.css';
import Sidebar from './Sidebar';
import Chat from './Chat';

function App() {
  return (
    <div className="App">
       <div className="Main">
                 {/* Sidebar (contacts, barre de recherche + menu profil) */}
         <Sidebar />
      {/* Component chat */}
         <Chat />
       </div>

    </div>
  );
}

export default App;
