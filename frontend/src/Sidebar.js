import React from 'react'
import "./Sidebar.css"
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import { Avatar, IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ChatIcon from '@material-ui/icons/Chat';
import SearchOutlined from '@material-ui/icons/SearchOutlined';
import SidebarChat from './SidebarChat';


function Sidebar() {
   return (
      <div className="sidebar">
         <div className="s-header">
            <Avatar src="https://avatars0.githubusercontent.com/u/27002047?s=60&v=4"/>
            <div className="s-header-droite">
               <IconButton>
                  <DonutLargeIcon />
               </IconButton>
               <IconButton>
                  <ChatIcon />
               </IconButton>
               <IconButton>
                  <MoreVertIcon />
               </IconButton>
               
            </div>
         </div>
         <div className="s-search">
            <div className="s-search-container">
               <SearchOutlined />
               <input placeholder="Rechercher..." type="text"/>
            </div>
         </div>

         <div className="s-chat">
            <SidebarChat />
         </div>
      </div>
   )
}

export default Sidebar
