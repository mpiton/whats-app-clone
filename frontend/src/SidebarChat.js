import React from 'react'
import "./SidebarChat.css"
import { Avatar } from '@material-ui/core'

function SidebarChat() {
   return (
      <div className="sidebar-chat">
         <Avatar />
         <div className="sc-info">
            <h2>Nom du salon</h2>
            <p>dernier message</p>
         </div>
      </div>
   )
}

export default SidebarChat
