import React from 'react'
import "./Chat.css"
import { Avatar, IconButton } from '@material-ui/core'
import {AttachFile, MoreVert, SearchOutlined} from '@material-ui/icons/'
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import MicIcon from '@material-ui/icons/Mic';

function Chat() {
   return (
      <div className="chat">
         <div className="chat-header">
            <Avatar/>

            <div className="ch-info">
               <h3>Nom de la room</h3>
               <p>Dernière vue le...</p>
            </div>

            <div className="ch-right">
               <IconButton>
                  <SearchOutlined/>
               </IconButton>
               <IconButton>
                  <AttachFile />
               </IconButton>
               <IconButton>
                  <MoreVert />
               </IconButton>
            </div>
         </div>

         <div className="chat-body">
            <p className="chat-message">
               <span className="chat-name">Mathieu</span>
               Ceci est un message
               <span className="chat-heure">{new Date().toUTCString()}</span>
            </p>

            <p className="chat-message chat-message-recu">
               <span className="chat-name">Mathieu</span>
               Ceci est un message
               <span className="chat-heure">{new Date().toUTCString()}</span>
            </p>
         </div>

         <div className="chat-footer">
            <InsertEmoticonIcon />
            <form>
               <input 
                  type="text"
                  placeholder="Tapez votre message"
               />
               <button type="submit">Envoyer un message</button>
            </form>
            <MicIcon />
         </div>
      </div>
   )
}

export default Chat
